# Preparando ambiente e executando #

* Clonar projeto ou baixar .zip;
* Instalar o compilador GCC utilizando o comando apt-get install gcc;
* Compilar o arquivo main.c utilizando o seguinte comando: gcc main.c -o main
* Ao utilizar o código acima, será gerado um arquivo texto.txt 
* O arquivo main é o binário do programa;
* O arquivo texto.txt é o resultado gerados das threads;

# Resultados #

* A solução apresentada no programa utiliza chaveamento obrigatório para regular o acesso das threads às suas regiões críticas, na qual a primeira thread é executada primeiro e altera a variável x para 1, para que a segunda thread seja executada em seguida.
* Sem o tratamento de uma condição de corrida, apenas uma das threads consiguem escrever no arquivo (texto.txt).
* O arquivo main.c apresenta o problema com a implementação de um chaveamento obrigatório, permitindo que ambas as threads gravem no arquivo (texto.txt), pois é regulado o acesso das mesmas às suas regiões críticas.