#define _GNU_SOURCE
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <malloc.h>
#include <sys/types.h>
#include <signal.h>
#include <sched.h>
#include <sys/wait.h>
#define FIBER_STACK 1024*64

FILE *arq;
int x=0;
int gravaArquivo(void *argument){
	arq = fopen("texto.txt","a");
	fprintf(arq,"thread 1.\n");
	fclose(arq);
	printf("Thread 1 terminada\n");
	x=1;
	return 0;
}
int gravaArquivo2(void *argument){
	arq = fopen("texto.txt","a");
	fprintf(arq,"thread 2.\n");
	fclose(arq);
	printf("Thread 2 terminada\n");
	x=0;
	return 0;
}
int main(){
	pid_t t1,t2;
	void *stack;
	stack = malloc(FIBER_STACK);

	t1=clone(&gravaArquivo,(char*)stack + FIBER_STACK,SIGCHLD|CLONE_FS|CLONE_FILES|CLONE_SIGHAND|
	CLONE_VM,0);

	while(x == 0){
		printf("Esperando thread\n");
	}
	//waitpid(t1,0,0); variavel de lock para resolver o problema, sem o wait somente a segunda thread consegue salvar o arquivo

	t2=clone(&gravaArquivo2,(char*)stack + FIBER_STACK,SIGCHLD|CLONE_FS|CLONE_FILES|CLONE_SIGHAND|
        CLONE_VM,0);

	free(stack);
	return 0;
}

